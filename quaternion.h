//
// Created by Zahra Forootaninia on 5/4/16.
//

#ifndef QUATERNIONS_FINALPROJ_QUATERNIONS_H
#define QUATERNIONS_FINALPROJ_QUATERNIONS_H

#include "math.h"


class myQuaternion {

public:


    float x;
    float y;
    float z;
    float w;

    myQuaternion() {

        x = 0.0;
        y = 0.0;
        z = 0.0;
        w = 1.0;
    }

    myQuaternion(const float in_x, const float in_y, const float in_z) {
        w = 0.0;

        // Calculate the x, y and z of the quaternion
        x = in_x;
        y = in_y;
        z = in_z;

        normalizeQuat();
    }

    myQuaternion(const float in_x, const float in_y, const float in_z, const float in_degrees) {

        float angle = float((in_degrees / 180.0f) * M_PI);
        float result = float(sin(angle / 2.0f));
        w = float(cos(angle / 2.0f));

        // Calculate the x, y and z of the quaternion
        x = float(in_x * result);
        y = float(in_y * result);
        z = float(in_z * result);

        normalizeQuat();
    }

    void normalizeQuat() {

        float mag = sqrt( pow(w,2) + pow(x,2) + pow(y,2) + pow(z,2));

        if(mag < 1e-5) {
            mag = 1.;
        }
        w = w/mag;
        x = x/mag;
        y = y/mag;
        z = z/mag;

    }

    void quaternionProduct(const myQuaternion &q, myQuaternion &q_out) {

        q_out.w = w * q.w - x * q.x - y * q.y - z * q.z;
        q_out.x = w * q.x + x * q.w + y * q.z - z * q.y;
        q_out.y = w * q.y + y * q.w + z * q.x - x * q.z;
        q_out.z = w * q.z + z * q.w + x * q.y - y * q.x;

        float mag = sqrt( pow(q_out.w,2) + pow(q_out.x,2) + pow(q_out.y,2) + pow(q_out.z,2));

        if(mag < 1e-5) {
            mag = 1.;
        }

        q_out.w = q_out.w/mag;
        q_out.x = q_out.x/mag;
        q_out.y = q_out.y/mag;
        q_out.z = q_out.z/mag;
    };

    void rotateVector(const float vec_in[], float vec_out[]) {


        myQuaternion q_inv;
        q_inv.w = w;
        q_inv.x = -x;
        q_inv.y = -y;
        q_inv.z = -z;

        myQuaternion p(vec_in[0], vec_in[1], vec_in[2]);


        myQuaternion p_pri;

        myQuaternion q_tmp;

        p.quaternionProduct(q_inv, q_tmp);
        quaternionProduct(q_tmp, p_pri);

        vec_out[0] = p_pri.x;
        vec_out[1] = p_pri.y;
        vec_out[2] = p_pri.z;



    }


};


#endif //QUATERNIONS_FINALPROJ_QUATERNIONS_H
