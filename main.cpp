//
// Created by Zahra Forootaninia on 5/4/16.
//

#include "main.h"
#include "quaternion.h"
#include <iostream>
#include <iomanip>


#include "camera.h"
#include <math.h>
#include "opengl.h"
#include <stdio.h>
#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */
using namespace std;


struct bone {

    float pos[3];
    float v_base[3];
    float len;
    myQuaternion q;

    int parentId;
    int leftId;
    int rightId;
    int centerId;

    int id;
} ;

const int size = 19;
const float y_offset = 1.0;
const float h = 0.2;



bone bones[size];
float conections[size-1][6];



int temp_count = 0;


void setVec(float vec[], float x, float y, float z){

    float mag_v = sqrt(pow(x,2) + pow(y,2) + pow(z,2));
    if(mag_v < 1e-5) {
        mag_v = 1.;
    }
    vec[0] = x / mag_v;
    vec[1] = y / mag_v;
    vec[2] = z / mag_v;


    cout<<"mag: "<< mag_v<<endl;
}



void bones_init(){

    for(int i =1; i < size; i++ ){

        bones[i].len = h;
        bones[i].id = i;
        //cout<<"Assigning ID to "<<i<<endl;
    }


    // hip
    setVec(bones[0].v_base ,0., 0. ,0.);
    bones[0].len = 0.;
    bones[0].id = 0;

    // upper leg
    bones[0].leftId = 1;
    bones[0].rightId = 2;
    bones[0].centerId = 7;

    setVec(bones[1].v_base, -1., .5  ,0.);
    setVec(bones[2].v_base, 1., .5 ,0.);

    bones[1].parentId = 0;
    bones[2].parentId = 0;
    bones[1].len = bones[2].len = 0.8 * h;

    // lower leg
    bones[1].leftId = 3;
    bones[2].leftId = 4;

    setVec(bones[3].v_base, 0., 1.  ,0.);
    setVec(bones[4].v_base, 0., 1.  ,0.);
    bones[3].parentId = 1;
    bones[4].parentId = 2;

    bones[3].len = bones[4].len = 1.2 * h;


    //feet
    bones[3].leftId = 5;
    bones[4].leftId = 6;

    setVec(bones[5].v_base, 0., 1.  ,0.);
    setVec(bones[6].v_base, 0., 1.  ,0.);

    bones[5].parentId = 3;
    bones[6].parentId = 4;
    bones[6].len = bones[5].len = 2 * h;

    // sholders
    setVec(bones[7].v_base, 0., -1.  ,0.);
    bones[7].parentId = 0;
    bones[7].centerId = 16;
    bones[7].len = 2.5 * h;


    //upper arms
    bones[7].leftId = 9;
    bones[7].rightId = 8;


    setVec(bones[9].v_base, -1., 0.  ,0.);
    setVec(bones[8].v_base, 1., 0. ,0.);

    bones[8].parentId = 7;
    bones[9].parentId = 7;
    //bones[8].len = bones[9].len = 0.1 * h;


    // lower arm
    bones[8].leftId = 10;
    bones[9].leftId = 11;

    setVec(bones[10].v_base, 0., 1.  ,0.);
    setVec(bones[11].v_base, 0., 1.  ,0.);

    bones[10].parentId = 8;
    bones[11].parentId = 9;

    //wrest
    bones[10].leftId = 12;
    bones[11].leftId = 13;

    setVec(bones[12].v_base, 0., 1.  ,-1.);
    setVec(bones[13].v_base, 0., 1.  ,-1.);

    bones[12].parentId = 10;
    bones[13].parentId = 11;

    //hand
    bones[12].leftId = 14;
    bones[13].leftId = 15;

    setVec(bones[14].v_base, 0., 1.  ,-0.5);
    setVec(bones[15].v_base, 0., 1.  ,-0.5);

    bones[14].parentId = 12;
    bones[15].parentId = 13;
    bones[14].len = bones[15].len = 0.4 * h;


    // head
    setVec(bones[16].v_base, 0., -1.  ,0.);
    bones[16].parentId = 7;


}


void updatePositions(bone &b, myQuaternion q_cum, const float v_cum2[]){

    cout<<" updatePositoons for "<<b.id<<endl;

    float v_out[3] = {0.,0 ,0.};
    float v_cum[3] = {0.,0 ,0.};

    // Accumulate orintation for current bone
    b.q.quaternionProduct(q_cum, q_cum);

    // Orient bases bone
    q_cum.rotateVector(b.v_base, v_out);

    // Accumulate position vector
    v_cum[0] = v_cum2[0] - b.len * v_out[0];
    v_cum[1] = v_cum2[1] - b.len * v_out[1];
    v_cum[2] = v_cum2[2] - b.len * v_out[2];



    // Update position of current bone
    b.pos[0] = v_cum[0];
    b.pos[1] = v_cum[1];
    b.pos[2] = v_cum[2];

    if(b.leftId > 0) {
        //cout<<" calling leftId for  "<<b.leftId<<endl;

        updatePositions(bones[b.leftId], q_cum, v_cum);
    }

    if(b.rightId > 0) {
        //cout<<" calling rightId for  "<<b.rightId<<endl;

        updatePositions(bones[b.rightId], q_cum, v_cum);
    }

    if(b.centerId > 0) {
        //cout<<" calling centerId for  "<<b.centerId<<endl;
        updatePositions(bones[b.centerId], q_cum, v_cum);
    }



}



void updateConnections(bone bones[] ){

    myQuaternion q_cum;
    float v_cum[3] = {0., 0  ,0.};

    //cout<<"Calling updatePositoons "<<bones[].id<<endl;
    updatePositions(bones[0], q_cum, v_cum);


    for (int i = 1; i < size; i++) {
        // add the position of parent joint
        conections[i-1][0] = bones[bones[i].parentId].pos[0];
        conections[i-1][1] = bones[bones[i].parentId].pos[1] + y_offset;
        conections[i-1][2] = bones[bones[i].parentId].pos[2];
        // add the position of child joint
        conections[i-1][3] = bones[i].pos[0];
        conections[i-1][4] = bones[i].pos[1] + y_offset;
        conections[i-1][5] = bones[i].pos[2];
    }


}






int width = 800, height = 800; // size of window in pixels
Camera camera;        // camera

struct MouseInfo {
    bool dragging;    // is mouse being dragged?
    int xdrag, ydrag; // where the drag began
    int xprev, yprev; // where the mouse was last seen
} mouseInfo;

void drawCube() {
    glPushAttrib(GL_LIGHTING_BIT);
    glDisable(GL_LIGHTING);
    glColor3f(0.6,0.6,0.6);
    glBegin(GL_LINES);
    double x[] = {-1, 1}, y[] = {0, 2}, z[] = {-1, 1};
    for (int j = 0; j < 2; j++) {
        for (int k = 0; k < 2; k++) {
            glVertex3f(x[0], y[j], z[k]);
            glVertex3f(x[1], y[j], z[k]);
        }
    }
    for (int i = 0; i < 2; i++) {
        for (int k = 0; k < 2; k++) {
            glVertex3f(x[i], y[0], z[k]);
            glVertex3f(x[i], y[1], z[k]);
        }
    }
    for (int i = 0; i < 2; i++) {
        for (int j = 0; j < 2; j++) {
            glVertex3f(x[i], y[j], z[0]);
            glVertex3f(x[i], y[j], z[1]);
        }
    }
    glEnd();
    glPopAttrib();
}



void drawSpheres() {



    // Draw joints
    for (int s = 0; s < size; s++) {
        //float c = min(1.0, 0.8 * log10(3.0 * vel_color[s] + 0.1));
        glColor3f(0,0,0);
        glPushMatrix();
        glTranslatef(bones[s].pos[0], y_offset + bones[s].pos[1], bones[s].pos[2]);
        //glTranslatef(newBones[s].pos[0], y_offset + newBones[s].pos[1], newBones[s].pos[2]);

        glutSolidSphere(0.02, 20, 20);
        glPopMatrix();

    }

    // head
    glPushMatrix();
    glTranslatef(bones[16].pos[0], y_offset + bones[16].pos[1], bones[16].pos[2]);
    //glTranslatef(newBones[s].pos[0], y_offset + newBones[s].pos[1], newBones[s].pos[2]);

    glutSolidSphere(0.5* h, 20, 20);
    glPopMatrix();



    // draw bones
    glColor3f(0.137255, 0.137255, 0.556863); //navy
    glEnableClientState( GL_VERTEX_ARRAY );
    glVertexPointer( 3, GL_FLOAT, 0, conections );
    glDrawArrays( GL_LINES, 0, 2 * (size -1) );
    glDisableClientState( GL_VERTEX_ARRAY );
    glEnable(GL_LIGHTING);

}

void display() {
    glClearColor(1,1,1,1);
    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);
    glEnable(GL_COLOR_MATERIAL);
    glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE);
    glEnable(GL_NORMALIZE);
    camera.apply();
    drawCube();
    drawSpheres();
    //world.draw();
    glutSwapBuffers();
}

void reshape(int w, int h) {
    ::width = w;
    ::height = h;
    glViewport(0, 0, w, h);
}

float theta = 2;

void idle() {


    if (temp_count > 80){
//        theta = -theta;
        temp_count = 0;
    }

    // hit bones with this quat
    myQuaternion q_tweak_1(1, 0, 0, theta);
    myQuaternion q_tweak_2(1, 0, 0, -theta);


//   arms
    if (temp_count < 40) {
        bones[11].q.quaternionProduct(q_tweak_1, bones[11].q);
        bones[10].q.quaternionProduct(q_tweak_2, bones[10].q);

//        if (temp_count > 30){
//
//            bones[13].q.quaternionProduct(q_tweak_1, bones[13].q);
//            bones[12].q.quaternionProduct(q_tweak_2, bones[12].q);
//
//        }
    }
    else{
        bones[11].q.quaternionProduct(q_tweak_2, bones[11].q);
        bones[10].q.quaternionProduct(q_tweak_1, bones[10].q);

//        if (temp_count > 50){
//
//            bones[13].q.quaternionProduct(q_tweak_2, bones[13].q);
//            bones[12].q.quaternionProduct(q_tweak_1, bones[12].q);
//
//        }
    }

//  Legs
    if (temp_count < 40) {
        bones[3].q.quaternionProduct(q_tweak_1, bones[3].q);
        bones[4].q.quaternionProduct(q_tweak_2, bones[4].q);

        bones[5].q.quaternionProduct(q_tweak_2, bones[5].q);
        bones[6].q.quaternionProduct(q_tweak_1, bones[6].q);

//        if (temp_count > 30){
//
//            bones[13].q.quaternionProduct(q_tweak_1, bones[13].q);
//            bones[12].q.quaternionProduct(q_tweak_2, bones[12].q);
//
//        }
    }
    else{
        bones[3].q.quaternionProduct(q_tweak_2, bones[3].q);
        bones[4].q.quaternionProduct(q_tweak_1, bones[4].q);

        bones[5].q.quaternionProduct(q_tweak_1, bones[5].q);
        bones[6].q.quaternionProduct(q_tweak_2, bones[6].q);

//        if (temp_count > 50){
//
//            bones[13].q.quaternionProduct(q_tweak_2, bones[13].q);
//            bones[12].q.quaternionProduct(q_tweak_1, bones[12].q);
//
//        }
    }


    temp_count += 1;

    updateConnections(bones);

    glutPostRedisplay();
}

void mouse(int button, int state, int x, int y) {
    ::mouseInfo.xprev = x;
    ::mouseInfo.yprev = y;
    if (button == GLUT_LEFT_BUTTON) {
        if (state == GLUT_DOWN) {
            ::mouseInfo.dragging = true;
            ::mouseInfo.xdrag = x;
            ::mouseInfo.ydrag = y;
        } else if (state == GLUT_UP) {
            ::mouseInfo.dragging = false;
        }
    }
}

void motion(int x, int y) {
    camera.longitude -= (x - ::mouseInfo.xprev)*0.2;
    camera.latitude += (y - ::mouseInfo.yprev)*0.2;
    camera.latitude = min(max(camera.latitude, -90.), 90.);
    ::mouseInfo.xprev = x;
    ::mouseInfo.yprev = y;
}




// ################################################ Main ################################################

int main(int argc, char **argv) {

    bones_init();


    updateConnections(bones);


    camera = Camera(Vector3d(0,1,0), 6, 30);

    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_RGBA|GLUT_DOUBLE|GLUT_DEPTH|GLUT_MULTISAMPLE);
    glutInitWindowSize(::width, ::height);
    glutCreateWindow("Animation");
    glutDisplayFunc(display);
    glutReshapeFunc(reshape);
    glutIdleFunc(idle);
    glutMouseFunc(mouse);
    glutMotionFunc(motion);
    glutMainLoop();

    return 0;

}





